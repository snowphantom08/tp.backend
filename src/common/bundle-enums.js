const MessageStatusType = {
  SENT: 'SENT',
  RECEIVED: 'RECEIVED',
  SEEN: 'SEEN',
  FAILED: 'FAILED'
}

module.exports = {
  MessageStatusType
}