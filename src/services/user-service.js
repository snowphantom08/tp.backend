const { DuplicateException, TelepoundException, AuthenException, ValidateException } = require("../exceptions");
const { getDbConnection, dbCollections } = require("../infrastructure/db-manager");
const bcrypt = require('bcrypt');
const { User } = require("../models");
const jwt = require('jsonwebtoken')
const config = require('config');
const { formatPhone } = require("../utils/phone-util");

const registerToken = (data) => {
  const token = jwt.sign(data, config.tokenSecret, {
    expiresIn: config.tokenExpires
  })

  return token
}

const signUp = async (data) => {
  // Gen hash password
  const salt = await bcrypt.genSalt(12);
  const hashPass = await bcrypt.hash(data.password, salt);

  data.phone = data.phone && formatPhone(data.phone)

  // Update new hash password
  data.password = hashPass;

  // Insert new account to DB
  const user = new User(data)
  const result = await user.save()
  // const result = await User.insertMany([accountData]);
  
  // Remove unecessary fields and return result
  if (result) {
    const account = result.toObject()

    delete account.password

    const token = registerToken(account)

    return { ...account, token }
  }

  throw new TelepoundException('Something went wrong!')
}

const checkPhone = async (phone) => {  
  phone = formatPhone(phone)
  
  const findResult = await User.findOne({ phone }).select('+password')
  const account = findResult && findResult.toObject()

  return {
    exist: account?._id && true || false,
    account
  }
}

const login = async (data) => {
  const { password } = data
  const phone = data.phone && formatPhone(data.phone)
  const checkPhoneData = await checkPhone(phone)

  const { exist, account } = checkPhoneData

  // Do login - Compare password
  if (exist) {
    
    const valid = account?.password 
      && await bcrypt.compare(password, account.password)
      || false;    

    if (valid) {
      delete account.password;

      const token = registerToken(account)

      return { ...account, token }
    }

    throw new AuthenException('Please recheck your information.');
  }

  // If phone does not exist - Do sign up
  return await signUp(data)
}

const fetchProfile = async ({ _id, phone }) => {
  const result = await User.findOne({ _id });
  delete result?.password

  return result
}

const updateProfile = async (data) => {
  const { _id } = data
  const phone = data.phone && formatPhone(data.phone)

  const filter = (_id && { _id }) 
    || (phone && { phone })
    || {}
  
  const result = await User.findOneAndUpdate(
    filter,
    {
      $set: data
    },
    { new: true }
  )

  if (!result) {
    throw new TelepoundException('Update profile failed')
  }
  
  return result
}

const fetchUserChats = async ({ user, query, limit, offset }) => {
  offset = offset || 0
  limit = limit || 0

  const { _id } = user
  const filter = { _id }
  const result = await User
    .find(filter)
    .populate({
      path: 'chats',
      populate: {
        path: 'users'
      }
    })
    .populate('lastMessage')
    .limit(limit)
    .skip(offset)
    .select('chats')

  return result && result.chats
}

const fetchUsers = async ({ query, limit, offset } ) => {
  offset = offset || 0
  limit = limit || 5
  query = query?.trim()

  const result = await User
    .find({ phone: { $regex: `${query}` }})
    .limit(limit)
    .skip(offset)
    .select('phone name avatar')
    // .toArray()

  return result
}

const addChatToUsers = async (chat) => {
  if (!chat) {
    throw new ValidateException('Chat')
  }

  const result = []
  for (let participant of chat.participants) {
    const _id = participant.user._id || participant.user
    const updatedUser = await User.findOneAndUpdate(
      { _id },
      {
        $addToSet: {
          chats: chat._id
        }
      },
      {
        new: true
      }
    )

    result.push(updatedUser)
  }

  return result
}

const updateUserOnline = async (data) => {
  const { _id } = data
  const user = await User
    .findOneAndUpdate(
      { _id },
      {
        $set: {
          isOnline: true,
          lastOnline: new Date()
        }
      },
      {
        new: true
      }
    )
  
  return user?.toObject()
}

const updateUserOffline = async (data) => {
  const { _id } = data
  const user = await User
    .findOneAndUpdate(
      { _id },
      {
        $set: {
          isOnline: false,
          lastOnline: new Date()
        }
      },
      {
        new: true
      }
    )
  
  return user?.toObject()
}

module.exports = {
  checkPhone,
  login,
  fetchProfile,
  updateProfile,
  fetchUserChats,
  fetchUsers,
  addChatToUsers,
  updateUserOnline,
  updateUserOffline,
}