const { ObjectId } = require('bson')
const { ChatParticipant } = require('../models')

const updateChatParticipant = async (chatParticipant) => {
  chatParticipant._id = chatParticipant._id || new ObjectId()

  chatParticipant = await ChatParticipant.findOneAndUpdate(
    { _id: chatParticipant._id },
    {
      $set: chatParticipant
    },
    {
      upsert: true,
      new: true
    }
  )

  return chatParticipant
}

module.exports = {
  updateChatParticipant,
}