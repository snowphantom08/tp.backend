const { ObjectId } = require('bson')
const { Message, Chat, User } = require('../models')

const upsertMessage = async (message) => {
  message._id = message._id || new ObjectId() 
  const filter = { _id: message._id }

  message = await Message.findOneAndUpdate(
    filter,
    {
      $set: message 
    },
    {
      upsert: true,
      new: true
    }
  ).populate('medias').populate('user')

  return message
}

const getMessages = async ({ chat, offset, limit }) => {
  const filter = chat && { chat } || {}
  offset = offset || 0
  limit = limit || 350

  const result = await Message
    .find(filter)
    .populate('user')
    .populate('medias')
    .limit(limit)
    .skip(offset)
    .sort({ createdAt: 1 })
    
  return {
    chat,
    offset,
    limit,
    messages: result
  }
}

module.exports = { 
  upsertMessage,
  getMessages,
}