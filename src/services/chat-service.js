const { ObjectId } = require("bson")
const { TelepoundException } = require("../exceptions")
const { Chat, User } = require("../models")
const { createChatWithParticipants } = require("../utils/chat-util")
const { compareObjectById } = require("../utils/common-util")

const createChat = async (user, chat) => {
  chat._id = chat._id || new ObjectId()
  chat.users = [...chat.users, user]
  
  await Chat.insertMany(
    [chat],
  )

  return chat
}

const updateChat = async (chat) => {
  chat._id = chat._id || new ObjectId()
  
  chat = await Chat.findOneAndUpdate(
    { _id: chat._id },
    {
      $set: chat,
    },
    {
      upsert: true,
      new: true
    }
  )

  return chat
}

const getChats = async ({ user, query, offset, limit }) => {
  if (!user || !user._id) return []

  const filter = { _id: user._id }
  offset = offset || 0
  limit = limit || 0

  const foundUser = await User
    .findOne(filter)
    .limit(limit)
    .skip(offset)
    .select('chats')

  const chatIds = foundUser?.chats?.map(x => x._id) || []
  
  const result = await Chat
    .find({
      _id: {
        $in: chatIds
      }
    })
    .populate({
      path: 'participants',
      populate: {
        path: 'user',
        model: User
      }
    })
    .populate('lastMessage')
  
  const chats = result || []

  return chats
}

const getDirectChat = async (participants) => {
  try {
    if (!participants || participants.length < 2) return null

  const userIds = participants.map(x => (x.user._id || x.user))
  const filter = {
    'participants.user': {
      $all: userIds,
      // $size: 2
    },
    isGroupChat: false
  }

  let chat = await Chat
    .findOne(filter)
    .populate({
      path: 'participants',
      populate: {
        path: 'user',
        model: User
      }
    })
    .populate('lastMessage')
  
  if (!chat) {
    chat = createChatWithParticipants(participants)
    await updateChat(chat)
  } 

  return chat
  }catch(err) {
    console.log(err)
  }
}

const getChatParticipants = async (chat) => {
  if (!chat || !chat._id) return null

  const result = await Chat
    .findOne({
      _id: chat._id
    })
    .populate({
      path: 'participants',
      populate: {
        path: 'user',
        model: User
      }
    })
    .populate('lastMessage')

  const participants = (result && result.toObject()).participants

  return participants
}

const updateReadChat = async (chat, user) => {
  if (!chat || !user) return null

  const filter = { 
    _id: chat._id,
    'participants.user':  (user._id || user)
  }

  const result = await Chat
    .findOneAndUpdate(
      filter,
      {
        $set: {
          'participants.$.seenTime': new Date(),
          'participants.$.unreadCount': 0
        }
      },
      {
        new: true,
      }
    )
    .populate({
      path: 'participants',
      populate: {
        path: 'user',
        model: User
      }
    })
    .populate('lastMessage')
  
  const resultObj = result && result.toObject()

  return resultObj
}

const updateChatWithNewMessage = async (chat, message) => {
  if (!chat || !message) return null

  const { user } = message

  const participants = await getChatParticipants(chat)
  if (!participants || participants.length < 1) 
    throw new TelepoundException('updateChatWithNewMessage')
  
  const newParticipants = (participants || []).map(participant => {
    if (compareObjectById(participant.user, user)) {
      return {
        ...participant,
        seenTime: new Date(),
        unreadCount: 0
      }
    }

    const unreadCount = (participant?.unreadCount || 0) + 1
    return {
      ...participant,
      unreadCount
    }
  })

  const newChat = {
    ...chat,
    participants: newParticipants,
    lastMessage: message
  }

  await updateChat(newChat)

  return newChat
}

module.exports = {
  createChat,
  updateChat,
  getChats,
  getDirectChat,
  updateReadChat,
  updateChatWithNewMessage,
}