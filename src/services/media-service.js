const { ObjectId } = require('bson')
const cloudinary = require('cloudinary')
const { Media } = require('../models')

const upsertMedia = async (media) => {
  media._id = media._id || new ObjectId()
  
  media = await Media.findOneAndUpdate(
    { _id: media._id },
    {
      $set: media,
    },
    {
      upsert: true,
      new: true
    }
  )

  return media && media.toObject()
}

module.exports = {
  upsertMedia
}