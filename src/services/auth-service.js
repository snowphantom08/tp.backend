const verifyUserToken = async function (token) {
  if (!token) {
    throw new ValidationException('Null Token');
  }
  let decoded;
  try {
    token = token.replace('Bearer ', '');
    decoded = jwt.verify(token, config.app.tokenSecret);
  } catch (error) {
    throw new ValidationException('Invalid Token');
  }
  const user = await User.findOne({ where: { id: decoded.id } });
  if (!user) {
    throw new UnauthorizedException();
  }
  return decoded;
};