const DuplicateException = require('./duplicate-exception')
const TelepoundException = require('./telepound-exception')
const ValidateException = require('./validate-exception')
const AuthenException = require('./authen-exception')

module.exports = {
  DuplicateException,
  TelepoundException,
  ValidateException,
  AuthenException
}
