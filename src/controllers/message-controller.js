const { MessageService } = require("../services")

const fetchMessages = async (req, res, next) => {
  try {
    const { chat, offset, limit } = req.query; 
    const result = await MessageService.getMessages({ chat, offset, limit })

    req.data = result

    next()
  } catch (err) {
    next(err)
  }
}

const newMessage = async (req, res, next) => {
  try {
    const { text, user, media } = req.body;
    const result = await MessageService.upsertMessage({
      user,
      text,
      media
    });

    req.data = result
    req.message = 'Add new message successfully '

    next()
  } catch (err) {
    next(err)
  }
}

module.exports = {
  fetchMessages,
  newMessage
}