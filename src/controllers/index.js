const MessageController = require('./message-controller');
const UserController = require('./user-controller');
const ChatController = require('./chat-controller')
const MediaController = require('./media-controller')

module.exports = {
  MessageController,
  UserController,
  ChatController,
  MediaController,
}