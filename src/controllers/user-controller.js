const { UserService } = require("../services")
const { validatePhoneNumber } = require("../validates/phone-validation")

const checkPhone = async (req, res, next) => {
  try {
    const { phone } = req.body
    validatePhoneNumber(phone)
    
    const result = await UserService.checkPhone(phone)

    delete result.account

    req.data = result
    
    next()
  } catch(err) {
    next(err)
  }
}

const login = async (req, res, next) => {
  try {
    const result = await UserService.login(req.body)

    req.data = result
    req.message = 'Login successfully'

    next()
  } catch (err) {
    next(err)
  }
}

const fetchProfile = async (req, res, next) => {
  try {
    const { user } = req
    const result = await UserService.fetchProfile(user)
    
    req.data = result
    req.message = 'Get profile information successfully'
    
    next()
  } catch (err) {
    next(err)
  }
}

const updateProfile = async (req, res, next) => {
  try {
    const result = await UserService.updateProfile(req.body)

    req.data = result
    req.message = 'Update profile successfully'

    next()
  } catch (err) {
    next(err)
  }
}

const fetchUserChats = async (req, res, next) => {
  try {
    const result = await UserService.fetchUserChats(req.user)

    req.data = result
    req.message = 'Update profile successfully'

    next()
  } catch (err) {
    next(err)
  }
}

const searchUser = async (req, res, next) => {
  try {
    const { query, limit, offset } = req.query
    const result = await UserService.fetchUsers({ query, limit, offset })

    req.data = result

    next()
  } catch (err) {
    next(err)
  }
}

module.exports = {
  checkPhone,
  login,
  fetchProfile,
  updateProfile,
  fetchUserChats,
  searchUser,
}