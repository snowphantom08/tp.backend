const { ChatService, UserService } = require('../services')
const { addChatToUsers } = require('../services/user-service')
const { createChatWithParticipants } = require('../utils/chat-util')

const newChat = async (req, res , next) => {
  try {
    const chat = await ChatService.createChat(req.user, req.body)
    await UserService.addChatToUsers(chat)

    req.data = chat
    
    next()
  } catch (err) {
    next(err)
  }
}

const updateChat = async (req, res, next) => {
  try {
    const chat = await ChatService.createChat(req.user, req.body)
    await UserService.addChatToUsers(req.user, chat)

    req.data = chat
    
    next()
  } catch (err) {
    next(err)
  }
}

const fetchChats = async (req, res, next) => {
  try {
    const { user } = req
    const { offset, limit } = req.body
    const result = await ChatService.getChats({ user, offset, limit })

    req.data = result
    
    next()
  } catch (err) {
    next(err)
  }
}

const searchChat = async (req, res, next) => {
  try {
    const { user } = req
    const { offset, limit, query } = req.query

    const userDatas = await UserService.fetchUsers({ query, offset, limit })
    const chats = userDatas.map((data) => createChatWithParticipants([user, data]))

    req.data = chats

    next()
  } catch (err) {
    next(err)
  }
}

const getDirectChat = async (req, res, next) => {
  try {
    const { users } = req.body
    
    const chat = await ChatService.getDirectChat(users)
    await addChatToUsers(chat)

    req.data = chat

    next()
  } catch (err) {
    next(err)
  }
}

module.exports = {
  fetchChats,
  newChat,
  updateChat,
  searchChat,
  getDirectChat,
}