const { Media } = require("../models")
const { MediaService, FileService } = require("../services")
const formidable = require('formidable')
const { ValidateException } = require("../exceptions")

const uploadMedia = async (req, res, next) => {
  try {
    const form = new formidable.IncomingForm()
    const file = await new Promise(async (resolve, reject) => {
      form.parse(req, (err, fields, files) => {
        if (err) reject(err)

        const file = files.file
        resolve(file)
      })
    })
    if (!file) throw new ValidateException('File')
    const { mimetype } = file
    let mediaType = 'file'
    if (mimetype.includes('video')) {
      mediaType = 'video'
    } else if (mimetype.includes('image')) {
      mediaType = 'image'
    }

    const result = await FileService.upload(file)
    const media = new Media({
      src: result,
      mediaType,
      mimeType: mimetype,
      title: file.originalFilename
    })
    await media.save()

    req.data = media

    next()
  } catch (err) {
    next(err)
  }
}

const fetchSignedUrl = async (req, res, next) => {
  try {
    const data = FileService.getSignedUrl()
    
    req.data = data

    next()
  } catch (err) {
    next(err)
  }
}

module.exports = {
  uploadMedia,
  fetchSignedUrl
}