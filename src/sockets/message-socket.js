const { Server, Socket } = require('socket.io')
const { MessageStatusType } = require('../common/bundle-enums')
const { Message } = require('../models')
const { MessageService, ChatService, MediaService } = require('../services')

/**
 * 
 * @param {Server} io 
 * @param {Socket} socket 
 * @param {any} chats 
 * @param {any} cb 
 */
const sendMessage = async (io, socket, data, cb) => {
  try {
    const { chat, medias } = data
    const { _id: room } = chat

    if (medias) {
      for (const media of medias) {
        await MediaService.upsertMedia(media)
      }
    }

    const m = await MessageService.upsertMessage({
      ...data,
      status: MessageStatusType.SENT
    })

    const c = await ChatService.updateChatWithNewMessage(chat, m)

    io.in(room).emit('@message', m)
    io.in(room).emit('@chat', c)

    cb({
      data: m,
    })
  } catch (err) {
    cb({
      error: err
    })
  }
}

/**
 * Client call this when init their app
 * @param {Server} io 
 * @param {Socket} socket 
 * @param {any} chats 
 * @param {any} cb 
 */
 const fetchMessages = async (io, socket, data, cb) => {
  try {
    const { chat, offset, limit } = data; 
    const result = await MessageService.getMessages({ chat, offset, limit })

    cb({
      data: result
    })
  } catch (err) {
    cb({
      error: err
    })
  }
}

module.exports = {
  sendMessage,
  fetchMessages
}