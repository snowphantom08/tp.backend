const { Server, Socket } = require('socket.io')
const { ChatService, UserService } = require('../services')
const { createChatWithUser } = require('../utils/chat-util')
const { getSocketsByUser, getSocketIdsByUser } = require('./app-socket')

/**
 * Client call this when init their app
 * @param {Server} io 
 * @param {Socket} socket 
 * @param {any} chats 
 * @param {any} cb 
 */
const fetchUserChats = async (io, socket, data, cb) => {
  try {
    const { decoded_token } = socket
    const { offset, limit } = data || {}
    const result = await ChatService.getChats({ user: decoded_token, offset, limit })

    const chatIds = (result || []).filter(item => item._id).map(item => `${item._id}`)
    socket.join(chatIds)

    cb({
      data: result
    })
  } catch (err) {
    cb({ error: err.message })
  }
}

/**
 * 
 * @param {Server} io 
 * @param {Socket} socket 
 * @param {any} data 
 * @param {any} cb 
 */
const getDirectChat = async (io, socket, data, cb) => {
  try {
    const { decoded_token } = socket
    const { participants } = data
    
    const chat = await ChatService.getDirectChat(participants)
    await UserService.addChatToUsers(chat)

    for (const participant of chat.participants) {
      const { user } = participant

      const socketUserIds = getSocketIdsByUser(io, user) || []
      for (const id of socketUserIds) {
        io.sockets.sockets.get(`${id}`)?.join(`${chat._id}`)
      }
    }

    cb({
      data: chat
    })
  } catch (err) {
    cb({
      error: err.message
    })
  }
}

const searchChat = async (io, socket, data, cb) => {
  try {
    const { decoded_token : user } = socket
    const { offset, limit, query } = data

    const userDatas = await UserService.fetchUsers({ query, offset, limit })
    const chats = userDatas.map((x) => createChatWithUser([user, x]))

    cb({
      data: chats
    })
  } catch (err) {
    cb({
      error: err.message
    })
  }
}

const readChat = async (io, socket, data, cb) => {
  try {
    const { decoded_token : user } = socket
    const chat = data
    const { _id: room } = chat

    const newChat = await ChatService.updateReadChat(chat, user)
    io.in(room).emit('@chat', newChat)

    cb({
      data: newChat
    })
  } catch (err) {
    cb({
      error: err.message
    })
  }
}

const joinChat = (io, socket, data, cb) => {
  try {
    const { _id } = data || {}

    (_id && socket.join(`${_id}`))

    cb({
      data,
    })
  } catch (err) {
    cb({
      error: err.message
    })
  }
}

module.exports = {
  fetchUserChats,
  getDirectChat,
  searchChat,
  readChat,
  joinChat,
}