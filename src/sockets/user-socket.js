const { Server, Socket } = require('socket.io')
const { UserService } = require('../services')

/**
 * 
 * @param {Server} io 
 * @param {Socket} socket 
 * @param {any} data 
 * @param {any} cb 
 */
const fetchProfile = async (io, socket, data, cb) => {
  try {
    const { decoded_token } = socket
    const result = await UserService.fetchProfile(decoded_token)

    cb({
      data: result
    })
  } catch (err) {
    cb({ error: err.message })
  }
}

module.exports = {
  fetchProfile
}