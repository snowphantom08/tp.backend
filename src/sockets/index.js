const { Server } = require('socket.io')
const { appInit, appDisconenct } = require('./app-socket')
const config = require('config')
const { sendMessage, fetchMessages } = require('./message-socket')
const { fetchUserChats, getDirectChat, searchChat, readChat, joinChat } = require('./chat-socket')
const { fetchProfile } = require('./user-socket')

const socketHandleController = (handler, io, socket) => (...params) => handler(io, socket, ...params)
const SHC = socketHandleController

/**
 * 
 * @param {Server} io 
 */
module.exports = (io) => {
  // Binding event
  io.on('connection', async (socket) => {
    console.log(`👾 New socket connected! >> ${socket.decoded_token?.phone} : ${socket.id}, at: ${new Date().toLocaleString()}`,  )

    await appInit(io, socket, null, () => {})

    socket.on('disconnect', SHC(appDisconenct, io, socket))

    // User
    socket.on('@fetchProfile', SHC(fetchProfile, io, socket))

    // Chat
    socket.on('@fetchUserChats', SHC(fetchUserChats, io, socket))
    socket.on('@selectChat', SHC(getDirectChat, io, socket))
    socket.on('@searchChat', SHC(searchChat, io, socket))
    socket.on('@readChat', SHC(readChat, io, socket))
    socket.on('@joinChat', SHC(joinChat, io, socket))

    // Message
    socket.on('@fetchMessages', SHC(fetchMessages, io, socket))
    socket.on('@sendMessage', SHC(sendMessage, io, socket))
  })
}