const { Server, Socket } = require('socket.io')
const { ChatService, UserService } = require('../services')
const { compareObjectById } = require('../utils/common-util')

const userSockets = {}
/**
 * Client call this when init their app
 * @param {Server} io 
 * @param {Socket} socket 
 * @param {any} chats 
 * @param {any} cb 
 */
const appInit = async (io, socket, data, cb) => {
  try {
    const { decoded_token } = socket

    const userId = decoded_token?._id
    if (userId) {
      const sockets = userSockets[userId] || []
      sockets.push(socket.id)
      userSockets[userId] = [...new Set(sockets)]

      const newUser = await UserService.updateUserOnline(decoded_token)
      io.of('/').emit('@user', newUser)
    }

    cb({})
  } catch (err) {
    cb({ err: err.message })
  }
}

const appDisconenct = async (io, socket, data, cb) => {
  const { decoded_token } = socket
  const userId = decoded_token?._id
  if (userId) {
    const index = (userSockets[userId] || [])
      .map(x => `${x}`)
      .indexOf(`${socket.id}`)
    
    if (index > -1) {
      userSockets[userId].splice(index, 1)

      if (userSockets[userId].length < 1) {
        const newUser = await UserService.updateUserOffline(decoded_token)
        io.of('/').emit('@user', newUser)
      }
    }
  }
}

/**
 * Client call this when init their app
 * @param {Server} io 
 * @param {Socket} socket 
 * @param {any} chats 
 * @param {any} cb 
 */
const login = (io, socket, data, cb) => {
  try {
    cb({})
  } catch (err) {
    cb({ err: err.message })
  }
}

/**
 * 
 * @param {Server} io 
 * @param {String} userId
 * @returns {Array<Socket>}
 */
const getSocketsByUser = (io, user, cb) => {
  const userId = user?._id || user
  if (!userId) return []

  const result = (userSockets[userId] || [])
    .map(socketId => io.sockets.sockets.get(socketId))
    .filter(x => !!x)

  return result
}

const getSocketIdsByUser = (io, user) => {
  const userId = user?._id || user
  if (!userId) return []

  const result = (userSockets[userId] || [])

  return result
}

module.exports = {
  appInit,
  login,
  getSocketsByUser,
  getSocketIdsByUser,
  appDisconenct
}