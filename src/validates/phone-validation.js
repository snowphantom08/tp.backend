const { ValidateException } = require("../exceptions")
const { isValidPhoneNumber } = require("../utils/phone-util")

function validatePhoneNumber(phone) {
  if (!isValidPhoneNumber(phone)) {
    throw new ValidateException('Phone number')
  }
}

module.exports = {
  validatePhoneNumber
}
