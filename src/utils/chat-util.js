const { Chat } = require("../models");
const { ObjectId } = require("bson")

function createChatWithParticipants(participants) {
  const isGroupChat = participants && participants.lengsth > 2
  const chat = {
    _id: new ObjectId(),
    participants,
    isGroupChat,
  }

  return chat
}

function createChatWithUser(users) {
  if (!users || users.length < 2) return null
  const participants = users.map(user => ({
    user
  }))

  const isGroupChat = users && users.lengsth > 2
  const chat = {
    _id: new ObjectId(),
    participants,
    isGroupChat,
  }

  return chat
}

const updateChatInfo = async (chat, chatInfo) => {
  if (!chat || !chatInfo) return null

  const { user } = chatInfo
  const { participants } = chat
  const index = (participants || []).findIndex((x) => (x.user._id || x.user) === user._id)

  if (index !== -1) {
    participants[index] = chatInfo
  }
  
  return {
    ...chat,
    participants
  }
}

module.exports = {
  createChatWithParticipants,
  createChatWithUser,
  updateChatInfo
}