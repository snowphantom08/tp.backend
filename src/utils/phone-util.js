const PHONE_REGEX = /(\+\d{1,3})?\d{2,6}[\s.-]?\d{3,7}/
const { ValidateException } = require('../exceptions')

function isValidPhoneNumber(phoneNumber) {
  if (!phoneNumber) return false

  let isBad = !phoneNumber.match(PHONE_REGEX)
  if (!isBad) {
    phoneNumber = phoneNumber.replace(/\D/g, '')
    if (phoneNumber.length < 7 || phoneNumber.length > 16) {
      isBad = true
    }
  }

  return !isBad
}

function formatPhone(phone) {
  if (!phone) return phone

  const formatted = `+${phone.replace(/ /g, '').replace('+', '').toLowerCase()}` 
  if (!isValidPhoneNumber(formatted))
    throw new ValidateException('Phone number')

  return formatted
}

module.exports = {
  isValidPhoneNumber,
  formatPhone,
  PHONE_REGEX
}