const Mongoose = require('mongoose')
const { PHONE_REGEX } = require('../utils/phone-util')
const { Schema } = Mongoose

const userSchema = new Schema(
  {
    phone: {
      type: String,
      match: PHONE_REGEX,
      required: true
    },
    password: {
      type: String,
      required: true,
      min: 6,
      select: false
    },
    name: {
      type: String
    },
    chats: [
      {
        type: Mongoose.Schema.Types.ObjectId,
        ref: 'Chat'
      }
    ],
    lastOnline: {
      type: Date
    },
    isOnline: {
      type: Boolean,
      default: false
    }
  },
  { 
    timestamps: true,
    versionKey: false
  }
)

module.exports = Mongoose.model("User", userSchema)
