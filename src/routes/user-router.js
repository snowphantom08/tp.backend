const { UserController } = require('../controllers');
const { AuthMiddleware } = require('../middlewares');

const router = require('express').Router()

// Login 
router.post('/checkphone', UserController.checkPhone)
router.post('/login', UserController.login)

// Profile
router.get('/profile', AuthMiddleware.verifyUser, UserController.fetchProfile)
router.put('/profile', AuthMiddleware.verifyUser, UserController.updateProfile)

// Init
router.get('/chats', AuthMiddleware.verifyUser, UserController.fetchUserChats)

// Search
router.get('/search', AuthMiddleware.verifyUser, UserController.searchUser)

module.exports = router;
