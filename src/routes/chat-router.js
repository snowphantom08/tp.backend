const { ChatController } = require('../controllers')
const { AuthMiddleware } = require('../middlewares')

const router = require('express').Router()

router.get('/', AuthMiddleware.verifyUser, ChatController.fetchChats)
router.post('/', AuthMiddleware.verifyUser, ChatController.newChat)
router.get('/search', AuthMiddleware.verifyUser, ChatController.searchChat)
router.post('/direct', AuthMiddleware.verifyUser, ChatController.getDirectChat)

module.exports = router