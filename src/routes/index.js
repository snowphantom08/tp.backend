const router = require('express').Router();
const userRouter = require('./user-router');
const messageRouter = require('./message-router');
const chatRouter = require('./chat-router')
const mediaRouter = require('./media-router')
const { ResponseHandleMiddleware, ErrorLogsMiddleware, ErrorHandleMiddleware } = require('../middlewares');

router.use('/user', userRouter);
router.use('/message', messageRouter);
router.use('/chat', chatRouter)
router.use('/media', mediaRouter)

router.use(ResponseHandleMiddleware, ErrorLogsMiddleware, ErrorHandleMiddleware);

module.exports = router;
