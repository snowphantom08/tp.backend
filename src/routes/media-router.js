const { MediaController } = require('../controllers')
const { AuthMiddleware } = require('../middlewares')

const router = require('express').Router()

router.post('/', AuthMiddleware.verifyUser, MediaController.uploadMedia)
router.get('/signedUrl', AuthMiddleware.verifyUser, MediaController.fetchSignedUrl)

module.exports = router