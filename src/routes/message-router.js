const { MessageController } = require('../controllers');
const { AuthMiddleware } = require('../middlewares');

const router = require('express').Router();

router.get('/', AuthMiddleware.verifyUser, MessageController.fetchMessages);
router.post('/', AuthMiddleware.verifyUser, MessageController.newMessage);

module.exports = router;