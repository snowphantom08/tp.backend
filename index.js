process.env.NODE_CONFIG_DIR = './src/config';
const fs = require('fs');
const config = require('config');
const express = require('express');
const cors = require('cors');
var http = require('http');


//#region IMPORT ROUTER
const routes = require('./src/routes/index');
const { ErrorLogsMiddleware, ErrorHandleMiddleware, ResponseHandleMiddleware } = require('./src/middlewares');
const { connectDB } = require('./src/infrastructure/db-manager');
const { attachServer } = require('./src/infrastructure/socket-manager');
//#endregion

const app = express();
var server = http.createServer(app);

// Create logs folder if not exist

//#region SET UP MIDDLEWARE
app.use(cors(config.app.cors));
app.use(express.json({ limit: '100kb' }));
app.use(express.urlencoded({ limit: '100kb', extended: true }));
//#endregion

// Connect to DB
connectDB()

//#region SET UP ROUTER
app.use('/api/v1', routes);
//#endregion

const port = config.port || process.env.PORT || 5000
const host = config.host || process.env.HOST || 'localhost'
server.listen(port, () =>
  console.log(`Server running on ${host}:${port}`)
);

require('./src/sockets')(attachServer(server))
require('./src/infrastructure/app-manager')(app)
